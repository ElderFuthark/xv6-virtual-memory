#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "mmu.h"
#include "proc.h"
#include "sysfunc.h"
#include "pstat.h"

int numberOfSysCalls = 0;	// Extern int declared in syscall.c
int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since boot.
int
sys_uptime(void)
{
  uint xticks;
  
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

//Return number of system calls made since starting.
int sys_howmanysys(void)
{
  return numberOfSysCalls;
}
// Define sys_settickets
int sys_settickets(void)
{
	int raiseTickets;
	if (argint(0, &raiseTickets) < 0)
	{
		return -1; 	//Each process is assigned 1 ticket by default
	}
	else{
		return settickets(raiseTickets);	// Defined in proc.c
	}
}
// Define sys_getpinfo
int sys_getpinfo(void)
{
	struct pstat *pInfo;
	if(argptr(0, (void *)&pInfo, sizeof(*pInfo)) < 0){
		return -1;
	}
	if(pInfo == NULL){
		return -1;
	}			
	getpinfo(pInfo);				// Defined in proc.c
	return 0;
}
// Define sys_shmem_access
int sys_shmem_access(void)
{
	int page_number;
	void* shmemAddrVal = NULL;
	argint(0, &page_number);
	if (page_number < 0 || page_number > 3)
	{
		return NULL;
	}
	shmemAddrVal = shmem_access(page_number);
	return (int) shmemAddrVal;
}
// Define sys_shmem_count. Returns the value of shmem_count[page_number] on success and -1 on failure.
int sys_shmem_count(void)
{
	int page_number;
	if (argint(0, &page_number) < 0)
	{
		return -1;
	}
	if(page_number < 0 || page_number > 3)
	{
		cprintf("Invalid page number! Pages range from 0 to 3.\n");
		return -1;
	}
	return shmem_count(page_number);
}
