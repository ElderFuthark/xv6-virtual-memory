#include "types.h"
#include "user.h"

//Prints the name of the program and its arguments.
int main(int argc, char *argv[]){
  int current_arg;
  for (current_arg = 0; current_arg < argc; current_arg++){
    printf(1, argv[current_arg]);
    printf(1, " ");
  }
  printf(1, "\n");
  exit();
}
