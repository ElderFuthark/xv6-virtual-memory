#include "types.h"
#include "user.h"
#include "fcntl.h" 

#define PGSIZE 4096
#define USERTOP 0xA0000
#define INFO()\
	   printf(1, "failure in  %s : %s : %d\n", __FILE__, __func__, __LINE__);

// Function prototypes
int shmem_access_invalid_input();
int shmem_access_return_val();
int shmem_access_double_call();
int shmem_access_double_call_fork();
int shemem_access_readable_writable();
int shmem_access_ipc_test();
int shemem_access_full_addr_space();
int shmem_access_full_address_space2();
int shmem_access_persistence();
int shmem_access_exec();
int shmem_access_exec2();
int shmem_access_syscall_args();
int shmem_count_invalid_input();
int shmem_count_zero();
int shmem_count_zero2();
int shmem_count_one();
int shmem_count_one2();
int shmem_count_fork();
int shmem_count_exec();
	   
int main()
{
	
	/*
	int pgNum = 0;
	// Test shmem_access
	shmem_access(pgNum);
	// Test shmem_count
	printf(1, "Shared page %d has been mapped %d times.\n", pgNum, shmem_count(0));
	*/
	
//	shmem_access_invalid_input();
//	shmem_access_return_val();
//	shmem_access_double_call();
//	shmem_access_double_call_fork();
//	shemem_access_readable_writable();
//	shmem_access_ipc_test();
//	shemem_access_full_addr_space();
//	shmem_access_full_address_space2();
//	shmem_access_persistence();
//	shmem_access_exec();
//	shmem_access_exec2();
//	shmem_access_syscall_args();
//	shmem_count_invalid_input();
//	shmem_count_zero();
//	shmem_count_zero2();
//	shmem_count_one();
//	shmem_count_one2();
//	shmem_count_fork();
//	shmem_count_exec();
	
	exit();

	// Test shmem_access
}
void test_failed()
{
	printf(1, "FAILED!\n");
	exit();
}
void test_passed()
{
	printf(1, "PASSED!\n");
	//exit();
}

int shmem_access_invalid_input()
{
	void* ptr;
	ptr = (void*) (shmem_access(-1));
	if(ptr != NULL)
	{
		INFO();
		test_failed();
	}
	ptr = (shmem_access(-400));
	if(ptr != NULL)
	{
		INFO();
		test_failed();
	}
	ptr = (shmem_access(4));
	if(ptr != NULL)
	{
		INFO();
		test_failed();
	}
	ptr = (shmem_access(100));
	if(ptr != NULL)
	{
		INFO();
		test_failed();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}
// shmem_access return value should be one of 4 pages
// The address should be the first virtual page for the first shared page, the second virtual page for the second shared page, and so on.
int shmem_access_return_val()
{
	void* ptr;
	ptr = shmem_access(2);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	if(((int) ptr) != USERTOP - PGSIZE*1)
	{
		INFO();
		test_failed();
	}

	ptr = shmem_access(3);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	if(((int) ptr) != USERTOP - PGSIZE*2)
	{
		INFO();
		test_failed();
	}

	ptr = shmem_access(0);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	if(((int) ptr) != USERTOP - PGSIZE*3)
	{
		INFO();
		test_failed();
	}

	ptr = shmem_access(1);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	if(((int) ptr) != USERTOP - PGSIZE*4)
	{
	   INFO();
	   test_failed();
	}

	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_access must be able to map the same virtual address for multiple calls of the same page.
int shmem_access_double_call()
{
	void *ptr;
	void *ptr2;
	ptr = shmem_access(3);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	ptr2 = shmem_access(3);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	if(ptr != ptr2)
	{
		INFO();
		test_failed();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_access is called once in the parent process. It is then called again after fork in child process. The address received should be the same as the parent process.
int shmem_access_double_call_fork()
{
	void* ptr;
	ptr = shmem_access(3);
	//printf(1,"parent ptr = %d\n", (int) ptr);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();
	   	test_failed();
	}
	else if (pid == 0)
	{
		void *ptr2;
		ptr2 = shmem_access(3);
		//printf(1,"child ptr2 = %d\n", (int) ptr2);
		if(ptr2 == NULL)
		{
			INFO();
			test_failed();
		}
		if(ptr != ptr2)
		{
	   		 INFO();
	   		 test_failed();
		}
		return 0;
	}
	else
		wait();
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// Make sure shared pages are readable and writable
int shemem_access_readable_writable()
{
	char *ptr;
	int i;
	for(i=0; i<4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
		char c = 'c';
		ptr[100] = c;	// write
		char d = *(ptr+100); // read
		if (c != d)
		{
	   		INFO();
	   		test_failed();
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// One process writes and another process reads
int shmem_access_ipc_test()
{
	char *ptr;
	int i;
	char arr[8] = "COP4610";
	ptr = shmem_access(2);
	if(ptr == NULL)
	{
		INFO();	
		test_failed();
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();	
		test_failed();
	}
	else if(pid == 0)
	{
		sleep(20);
		for(i=0; i<7; i++)
		{
			if(*(ptr+i) != arr[i])
			{
				INFO();	
				test_failed();
			}
		}
		return 0;
	}
	else
	{
		for(i=0; i<7; i++)
		{
			*(ptr+i) = arr[i];
		}
		wait();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_access should fail if the entire address space is used.
int shemem_access_full_addr_space()
{
	void *ptr;
	while(1)
	{
		ptr = sbrk(1024);
		if(ptr == (char *) -1)
			break;
	}
	ptr = shmem_access(0);
	if(ptr != NULL)
	{
		INFO();
		test_failed();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}
// sbrk should not allocate and go past shared memory

int shmem_access_full_address_space2()
{
	void *ptr;
	ptr = shmem_access(0);
	if(ptr == NULL)
  	{
		INFO();	
		test_failed();
  	}
	void *ptr2;
	while (1)
	{
		ptr2 = sbrk(1024);
		if(ptr2 >= ptr && ptr2 != (char *) -1)
		{
			INFO();	
			test_failed();
		}
		if(ptr2 == (char *) -1)
			break;
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// make sure shared pages are not deallocated when no process references them.
int shmem_access_persistence()
{
	char *ptr;
	int i;
	char arr[8] = "COP4610";
	int pid = fork();
	if(pid < 0)
	{
		INFO();	
		test_failed();
	}
	else if (pid == 0)
	{
		ptr = shmem_access(3);
		if(ptr == NULL)
  		{
			INFO();	
			test_failed();
  		}
		for(i=0; i<7; i++)
			*(ptr+i) = arr[i];
		return 0;
	}
	else
	{
		wait();
		ptr = shmem_access(3);
		if(ptr == NULL)
  		{
			INFO();	
			test_failed();
  		}
		for(i=0; i<7; i++)
		{
			if(*(ptr+i) != arr[i])
  			{
				INFO();	
				test_failed();
  			}
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// A newly exec-ed program should not have access to shared pages when it begins.
char *args[] = { "echo_one", 0 };
int shmem_access_exec()
{
	void *ptr;
	int i;
	for(i=0; i<4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();	
			test_failed();
		}
	}
	int pid = fork();
	if(pid < 0)
		test_failed();
	else if (pid == 0)
	{
		exec("echo_one", args);
		printf(1, "exec failed!\n");
		INFO();
		test_failed();
		return 0;
	}
	else
		wait();
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// A newly exec-ed program should reallocate virtual pages if shmeme_access is called.
char *args2[] = { "echo_two", 0 };
int shmem_access_exec2()
{
	void *ptr;
	int i;
	for(i=0; i<4; i++)
	{
		ptr = shmem_access(i);
		//printf(1, "i = %d\n", i);
		if(ptr == NULL)
		{
			INFO();	
			test_failed();
		}
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();	
		test_failed();
	}
	else if (pid == 0)
	{
		exec("echo_two", args2);
		printf(1, "exec failed!\n");
		INFO();
		test_failed();
		return 0;
	}
	else
		wait();
	return 0;
}
// Make sure pointers frome shared pages can be successfully passed to system calls.
int shmem_access_syscall_args()
{
	char *ptr;
	int i;
	ptr = shmem_access(3);
	if(ptr == NULL)
	{
		INFO();
		test_failed();
	}
	char arr[4] = "tmp";
	for(i=0; i<4; i++)
		*(ptr+i) = arr[i];
	// argstr
	int fd = open(ptr, O_WRONLY|O_CREATE);
	if(fd == -1)
	{
		printf(1, "open system call failed to take a string from within a shared page\n");
		INFO();
		test_failed();
	}
	//argptr
	int n = write(fd, ptr, 10);
	if(n == -1)
	{
		printf(1, "write system call failed to take a pointer from within a shared page.\n");
		INFO();
		test_failed();
	}
	// making sure invalid strings are still caught
	int fd2 = open((char *)(USERTOP/2), O_WRONLY|O_CREATE);
	if(fd2 != -1)
	{
		printf(1, "write system call successfully accepted an ivalid string\n");
		INFO();
		test_failed();
	}
	// making sure invalid pointers are still caught
	n = write(fd, (char *)(USERTOP/2), 10);
	if(n != -1)
	{
		printf(1, "write system call successfully accepted an ivalid pointer\n");
		INFO();
		test_failed();
	}
	// make sure edge case is checked.
	n = write(fd, (char *)(ptr + 4094), 10);
	if(n != -1)
	{
		printf(1, "write system call successfully accepted an overflowing pointer in a shared page\n");
		INFO();
		test_failed();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

/// shmem_count tests ///
//invalid input test
int shmem_count_invalid_input()
{
	int n;
	n = shmem_count(-1);
	if(n != -1)
	{
		INFO();
		test_failed();
	}
	n = shmem_count(-100);
	if(n != -1)
	{
		INFO();
		test_failed();
	}
	n = shmem_count(4);
	if(n != -1)
	{
		INFO();
		test_failed();
	}
	n = shmem_count(100);
	if(n != -1)
	{
		INFO();
		test_failed();
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_count should return 0 if no one is referencing the page
int shmem_count_zero()
{
	int n;
	int i;

	for(i=0; i<4; i++)
	{
		n = shmem_count(i);
		if(n != 0)
		{
			INFO();
			test_failed();
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_count should return 0 if no one is referencing the page
int shmem_count_zero2()
{
	void *ptr;
	int n;
	int i;

	for(i=0; i<4; i++)
	{
		n = shmem_count(i);
		if(n != 0)
		{
			INFO();
			test_failed();
		}
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();
		test_failed();
	}
	else if (pid == 0)
	{
		for(i=0; i<4; i++)
		{
			ptr = shmem_access(i);
			if(ptr == NULL)
			{
				INFO();
				test_failed();
			}
		}
		return 0;
	}
	else
	{
		wait();
		for(i=0; i<4; i++)
		{
			n = shmem_count(i);
			if(n != 0)
			{
				INFO();
				test_failed();
			}
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_count should return 1 after 1 access
int shmem_count_one()
{
	void *ptr;
	int n;
	int i;
	for(i=0; i < 4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
		n = shmem_count(i);
		if(n != 1)
		{
			INFO();
			test_failed();
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}
// shmem_count should return 1 after two access to the same page
int shmem_count_one2()
{
	void *ptr;
	int n;
	int i;
	for(i=0; i < 4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
		n = shmem_count(i);
		if(n != 1)
		{
			INFO();
			test_failed();
		}
	}
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// shmem_count should return 2 after fork if the parent process has access the shared page.
int shmem_count_fork()
{
	void* ptr;
	int n;
	int i;

	for(i=0; i<4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();
		test_failed();
	}
	else if (pid == 0)
	{
		for(i=0; i<4; i++)
		{
			n = shmem_count(i);
			if(n != 2)
			{
				INFO();
				test_failed();
			}
		}
		return 0;
	}
	else
		wait();
	printf(1, "%s ", __func__);
	test_passed();
	return 0;
}

// Exec should decrement count for shared pages. Parent accesses a shared page, then it forks. Child performs exec to start a new program. In the new program, the count for the shared page should be 1.
char *args3[] = {"echo_three", 0};
int shmem_count_exec()
{
	void* ptr;
	int n;
	int i;

	for(i=0; i<4; i++)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
		{
			INFO();
			test_failed();
		}
	}
	for(i=0; i<4; i++)
	{
		n = shmem_count(i);
		if(n != 1)
		{
			INFO();
			test_failed();
		}
	}
	int pid = fork();
	if(pid < 0)
	{
		INFO();
		test_failed();
	}
	else if (pid == 0)
	{
		for(i=0; i<4; i++)
		{
			n = shmem_count(i);
			if(n != 2)
			{
				INFO();
				test_failed();
			}
		}
		exec("echo_three", args3);
		printf(1, "exec failed!\n");
		INFO();
		test_failed();
		return 0;
	}
	else
		wait();
	return 0;
}

///// Helper functions used in tests /////
/*
// echo_one helper function.
int echo_one()	// echo function called on exec
{
	char *ptr = (char *)(USERTOP - 4096);
	*ptr = 'c';
	test_failed();
	return 0;
}

// echo_two helper function.
int echo_two()	// echo function called on exec
{
	char *ptr;
	int i;
	for(i=3; i>=0; i--)
	{
		ptr = shmem_access(i);
		if(ptr == NULL)
			test_failed();
		*ptr = 'c';
	}
	test_passed();
	return 0;
}

// echo_three.
int echo_three()
{
	int n;
	int i;
	for(i=0; i < 4; i++)
	{
		n = shmem_count(i);
		printf(1, "n = %d\n", n);
		if(n != 1)
			test_failed();
	}
	test_passed();
	return 0;
}
//////////////////////////////////////////
*/
